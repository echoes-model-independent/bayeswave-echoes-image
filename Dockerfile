FROM containers.ligo.org/lscsoft/bayeswave/conda-env
ARG CI_JOB_TOKEN
ARG CI_COMMIT_SHA
ARG BAYESWAVE_TAG

# http://label-schema.org/rc1/
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="bayeswave-runtime"
LABEL org.label-schema.description="BayesWave-echoes fork"
LABEL org.label-schema.url="https://docs.ligo.org/lscsoft/bayeswave/"
LABEL org.label-schema.vcs-url="https://git.ligo.org/echoes-model-independent/production_release_bw/bayeswave/-/tree/v1.0.7_echoes_reviewed"
LABEL org.label-schema.build-date="${BUILD_DATE}"
LABEL org.label-schema.vcs-ref="${CI_COMMIT_SHA}"

RUN git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@git.ligo.org/echoes-model-independent/production_release_bw/bayeswave.git && \
      cd bayeswave && \
      git checkout tags/${BAYESWAVE_TAG} -b ${BAYESWAVE_TAG} && \
      mkdir -p build && \
      pushd build &&  \
      cmake .. \
      -DCMAKE_INSTALL_PREFIX= \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=true && \
      cmake --build . -- VERBOSE=1 && \
      cmake --build . --target install && \
      popd && \
      pip install --no-cache /bayeswave/BayesWaveUtils && \
      rm -rf /bayeswave

## Directories we may want to bind
RUN mkdir -p /cvmfs /hdfs /hadoop /etc/condor /test

COPY docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
