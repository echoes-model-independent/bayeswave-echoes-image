# bayeswave-echoes-image

Docker container image for BayesWave echoes branch

To build:

1. Click CI pipelines (the rocket ship at in the gitlab sidebar)
1. Click "Run pipeline" (top right)
1. Click "Run pipeline".

Note: this is a separate repository just for the Dockerfile, you do not need to
select a different branch in the gitlab-CI UI.
